
# README for 6ring coordinates

## Purpose :
This repository contains coordinates for six-membered rings. 

## Files :

- PDB coordinates for canonical conformer positions of cyclohexane and glucose.

- Select PDB coordinates extracted from FEARCF simulations. These were then optimised with various semi-empirical methods and DFT (these optimised structure not shown here) as per http://pubs.acs.org/doi/full/10.1021/jp107620h Figure 5 and Table 5.

## Reference :
{barnett2010ring,
  title={Ring puckering: a metric for evaluating the accuracy of AM1, PM3, PM3CARB-1, and SCC-DFTB carbohydrate QM/MM simulations},
  author={Barnett, Christopher B and Naidoo, Kevin J},
  journal={The Journal of Physical Chemistry B},
  volume={114},
  number={51},
  pages={17142--17154},
  year={2010},
  publisher={American Chemical Society}
}

